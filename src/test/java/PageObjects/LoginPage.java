package PageObjects;

import elementMapper.LoginPageElementMapper;
import org.openqa.selenium.support.PageFactory;
import utils.Web;

public class LoginPage  extends LoginPageElementMapper {

    public LoginPage() {PageFactory.initElements(Web.getCurrentDriver(), this);}


    public void clickSignIn() {login.click();}

    public void clickEmail() {emailcreate.sendKeys("testecwi2@gmail.com");}

    public void clickBtnSubmitCreate() {submitcreate.click();}

    public void clickBtnMrs() {genderMrs.click();}

    public void insertFirstname() {customerfirstname.sendKeys("Teste");}

    public void insertLastname() {customerlastname.sendKeys("CWI");}

    public void insertPassword() {passwd.sendKeys("teste123");}

    public void insertDays() {days.sendKeys("10");}

    public void insertMonths() {months.sendKeys("May");}

    public void insertYears() {years.sendKeys("1983");}

    public void acceptNewsletter(){newsletter.click();}

    public void acceptOptin(){optin.click();}

    public void insertAddress1(){address1.sendKeys("Testeendereço");}

    public void insertCity(){city.sendKeys("Miami");}

    public void insertState(){state.sendKeys("Florida");}

    public void insertPostCode(){postcode.sendKeys("33145");}

    public void insertPhoneMobile(){phonemobile.sendKeys("+1 305-857-0885");}

    public void clickBtnRegister(){submitregister.click();}

    public String getMyAccount(){return validar.getText();}

}


