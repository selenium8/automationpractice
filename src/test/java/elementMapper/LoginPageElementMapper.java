package elementMapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageElementMapper {

    @FindBy(className = "login")
    public WebElement login;

    @FindBy(id = "email_create")
    public WebElement emailcreate;

    @FindBy(css = "#SubmitCreate")
    public WebElement submitcreate;

    @FindBy(id = "id_gender2")
    public WebElement genderMrs;

    @FindBy(id = "customer_firstname")
    public WebElement customerfirstname;

    @FindBy(id = "customer_lastname")
    public WebElement customerlastname;

    @FindBy(id = "passwd")
    public WebElement passwd;

    @FindBy(id = "days")
    public WebElement days;

    @FindBy(id = "months")
    public WebElement months;

    @FindBy(id = "years")
    public WebElement years;

    @FindBy(id = "newsletter")
    public WebElement newsletter;

    @FindBy(id = "optin")
    public WebElement optin;

    @FindBy(id = "address1")
    public WebElement address1;

    @FindBy(id = "city")
    public WebElement city;

    @FindBy(id = "id_state")
    public WebElement state;

    @FindBy(id = "postcode")
    public WebElement postcode;

    @FindBy(id = "phone_mobile")
    public WebElement phonemobile;

    @FindBy(css = "#submitAccount > span")
    public WebElement submitregister;

    @FindBy(css = "#header > div.nav > div > div > nav > div:nth-child(1) > a > span")
    public WebElement validar;

 }

