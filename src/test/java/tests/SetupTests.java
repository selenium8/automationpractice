package tests;

import PageObjects.LoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Web;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SetupTests extends BaseTests {

     @Test
    public void criarConta() {


         //iniciar página
         LoginPage login = new LoginPage();

         //clicar no botao Sign In
         login.clickSignIn();

         //inserir email
         login.clickEmail();

         //clicar em criar conta
         login.clickBtnSubmitCreate();

         //clicar em Mrs
         login.clickBtnMrs();

         //digitar primeiro nome
         login.insertFirstname();

         //digitar sobrenome
         login.insertLastname();

         //inserir senha
         login.insertPassword();

         //inserir data de nascimento
         login.insertDays();
         login.insertMonths();
         login.insertYears();

         //aceitar receber notificações

         login.acceptNewsletter();
         login.acceptOptin();

         //inserir endereço
         login.insertAddress1();
         login.insertCity();
         login.insertState();
         login.insertPostCode();

         //inserir telefone
         login.insertPhoneMobile();

         //clicar botão Register
         login.clickBtnRegister();

          //validar My Account
         assertEquals("Teste CWI", login.getMyAccount());
     }

}