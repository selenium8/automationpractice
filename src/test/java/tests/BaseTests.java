package tests;

import org.junit.After;
import org.junit.Before;
import utils.Utils;
import utils.Web;

public class BaseTests {

    @Before
    public void setup(){Web.loadPage(Utils.getBaseUrl());}

    @After
    public void tearDown(){Web.close();}
}
