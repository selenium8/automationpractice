**Desafio Web CWI**

Testes automatizados do e-commerce: http://automationpractice.com/

**Objetivo:**<br> Criar testes para o fluxo de criação de conta de um usuário do ecommerce http://automationpractice.com/<br>

**Recursos utilizados:**<br>
• Rest-Assured; <br>
• JUnit; <br>
• Java 8; <br>
• Selenium WebDriver <br>

**Como configurar o ambiente:** <br>
• Faça clone do projeto: https://gitlab.com/selenium8/automationpractice <br>
• Importe o projeto para sua IDE de preferência; <br>
• Necessário ter instalado o JDK8<br>

**Como executar a aplicação:**<br>
• O projeto deve ser executado a partir da classe SetupTests
